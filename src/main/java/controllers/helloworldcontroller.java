package controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class helloworldcontroller {

	@RequestMapping("/hello")
	public String getHelloworld() {
		
		return "Hello World";
	}
	
	@RequestMapping("/helloagain")
	public String helloagain() {
		return "Hello Again";
	}
	
	@RequestMapping("/hellohello")
	public String hellohelloController() {
		return "hello hello";
	}
}
