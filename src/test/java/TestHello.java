import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestHello {

    Hello hello;

    @Before
    public void init(){
        hello = new Hello();
    }

    @Test
    public void testWithHello(){
        assertEquals(5, hello.calc(1));
    }

    @Test
    public void testCalc(){
        assertNotEquals(10,hello.calc(3));
    }

    @Test
    public void testCalc2(){
        assertEquals(25,hello.calc(5));
    }

    @Test
    public void testName(){
        assertEquals("hello world", hello.name("hello world"));
    }

}
